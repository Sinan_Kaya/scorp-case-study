// @ts-check

import {APIWrapper, API_EVENT_TYPE} from "./api.js";
import {
    addMessage,
    animateGift,
    isPossiblyAnimatingGift,
    isAnimatingGiftUI,
    generateMessageDiv
} from "./dom_updates.js";

const api = new APIWrapper();
let firstAnimate = 0;

function setPriority (events) {
    events.forEach(function (event, i) {
        if (event.type == API_EVENT_TYPE.ANIMATED_GIFT) {
            event.priority = 1;
        }
        else if (event.type == API_EVENT_TYPE.MESSAGE) {
            event.priority = 2;
        }
        else {
            event.priority = 3;
        }
    })
    return events;
}

let messages = [];
api.setEventHandler(async (events) => {
    let data = await setPriority(events);
    if (data.length) {
        data.sort(function (e1, e2) {
            return e1.priority - e2.priority
        })
    }
    data.forEach(function (event, i) {
        if (event.type == API_EVENT_TYPE.ANIMATED_GIFT) {
            if (!firstAnimate) {
                animateGift(event)
                firstAnimate = 1;
            }
            else {
                if (isPossiblyAnimatingGift()) {
                    animateGift(event)
                }
            }
        }
        document.getElementById('messages').append(generateMessageDiv(event))
        if (event.type == API_EVENT_TYPE.MESSAGE) {
            messages.push(event)
        }
    })
    deleteMessage()
})

function deleteMessage () {
    messages.forEach(function (event, index) {
        if (Date.now() - event.timestamp.getTime() > 20000) {
            document.getElementById(event.id).remove()
            messages.splice(index, 1);
        }
    })
}

// NOTE: UI helper methods from `dom_updates` are already imported above.
